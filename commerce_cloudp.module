<?php

/**
 * @file
 * Drupal Commerce CloudPayments payment method.
 *
 * This module contains basic integration with CloudPayments
 * for Drupal Commerce.
 */

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_cloudp_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_cloudp'] = array(
    'title' => t('CloudPayment'),
    'display_title' => t('CloudPayment'),
    'description' => t('Оплата через сервис CloudPayment'),
    'active' => TRUE,
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * Implements hook_menu().
 */
function commerce_cloudp_menu() {

  $items['commerce_cloudp/result'] = array(
    'title' => 'Result of payment through CloudPayment Merchant',
    'page callback' => 'commerce_cloudp_result',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
    $items['commerce_cloudp/payment'] = array(
    'title' => 'Оплата',
    'page callback' => 'commerce_cloudp_payment',
    'page arguments' => array('payment'),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Payment method loader.
 *
 * Load payment method with settings from url arg to support multiple payment
 * method instance if need.
 *
 * @param string $instance_id
 *   Variable part of payment method instance.
 *
 * @return bool
 *   Payment method instance.
 */



function commerce_cloudp_payment(){
  if(isset($_POST)) {
  $post=$_POST;
$amount = (float)trim($post['amount'])/100;
  drupal_add_js('https://widget.cloudpayments.ru/bundles/cloudpayments', 'external');
drupal_add_js('jQuery(document).ready(function () { var widget = new cp.CloudPayments();
    widget.charge({ // options
            publicId: "'.$post['publicId'].'",  
            description: "'.t("Order payment "). $post['orderId'].'", 
            amount: '.$amount.', 
            currency: "'.$post['currency_code'].'", 
            invoiceId: "'.$post['orderId'].'", 
            accountId: "'.$post['email'].'", 
            data: {
                
            }
        },
        function (options) { // success
          window.location.replace("'.url($post['redirect_uri']).'");
        },
        function (reason, options) { // fail
          window.location.replace("'.url($post['error_uri']).'");
       
        });});', 'inline');

}
  
return t('Load payment...');
}

/**
 * Helper to validate $_POST data.
 *
 * @param mixed $data
 *   $_POST to be validated.
 * @param mixed $payment_method
 *   Drupal commerce payment method instance passed via url param.
 * @param bool $is_interaction
 *   Fallback call flag.
 *
 * @return bool|mixed
 *   Transaction according to POST data or due.
 */
function _commerce_cloudp_validate_post($data, $payment_method = FALSE, $is_interaction = TRUE) {
  // Exit now if the $_POST was empty.
  if (empty($data)) {
    watchdog('commerce_cloudp', 'Interaction URL accessed with no POST data submitted.', array(), WATCHDOG_WARNING);
    print 'bad data';
    drupal_exit();
  }

  return $transaction;
}

/**
 * Page callback: commerce_cloudp/result.
 *
 * @param mixed $payment_method
 *   Drupal payment method passed via url arg.
 * @param bool $is_result
 *   Fallback call flag.
 */
function commerce_cloudp_result($payment_method, $is_result = TRUE) {
  // Retrieve the SCI from $_POST if the caller did not supply an SCI array.
  // Note that Drupal has already run stripslashes() on the contents of the
  // $_POST array at this point, so we don't need to worry about them.
  $data = $_POST;

  $transaction = _commerce_cloudp_validate_post($data, $payment_method, $is_result);

  $transaction->message_variables = array();
  $transaction->remote_status = '';
  $transaction->payload = $data;
  $order = commerce_order_load($transaction->order_id);

  if ($is_result) {
    $remote_payment_method = isset($data['PaymentMethod']) ? $data['PaymentMethod'] : 'CloudPayment';
    $transaction->message_variables = array('!remote_payment_method' => $remote_payment_method);
    $transaction->message = t('Success payment via !remote_payment_method');
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    commerce_payment_redirect_pane_next_page($order);
  }
  else {
    $transaction->message = t('Fail payment via CloudPayment');
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    commerce_payment_redirect_pane_previous_page($order);
  }
  commerce_payment_transaction_save($transaction);

  if ($is_result) {
    print 'OK' . $transaction->transaction_id;
    drupal_exit();
  }
}

/**
 * Default settings getter.
 *
 * @return array
 *   Default settings array.
 */
function commerce_cloudp_default_settings() {
  return array(
    'publicId' => '',
  );
}

/**
 * Payment method callback: settings form.
 *
 * @param mixed $settings
 *   Payment method instance settings.
 *
 * @return array
 *   Settings form array.
 */
function commerce_cloudp_settings_form($settings = NULL) {
  $form = array();

  $settings = (array) $settings + commerce_cloudp_default_settings();

  $form['publicId'] = array(
    '#type' => 'textfield',
    '#title' => t('publicId'),
    '#description' => t('Your cloudpay publicId'),
    '#default_value' => $settings['publicId'],
    '#required' => TRUE,
  );

  return $form;
}


function commerce_cloudp_submit_form($payment_method, $pane_values, $checkout_pane, $order) {


  return $form;
}


function commerce_cloudp_redirect_form($form, &$form_state, $order, $payment_method) {
   $form['#action'] = url('commerce_cloudp/payment');
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $currency_code = $order_wrapper->commerce_order_total->currency_code->value();
 $form['#attributes'] = array(
    'name' => 'payment',
    'accept-charset' => 'UTF-8',
  );
  $form['#method'] = 'post';

  $settings = $payment_method['settings'];

   foreach ($settings as $name => $value) {
    if (empty($value)) {
      continue;
    }
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }

 
  $form['amount'] = array(
    '#type' => 'hidden',
    '#value' => $order_wrapper->commerce_order_total->amount->value(),
  );
    $form['orderId'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );
    $form['currency_code'] = array(
    '#type' => 'hidden',
    '#value' => $currency_code,
  );
   $form['email'] = array(
    '#type' => 'hidden',
    '#value' => $order->mail,
  );

 
  // адрес для редиректа после успешной оплаты
  $form['redirect_uri'] = array(
    '#type' => 'hidden',
    '#value' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
  );
 
  // адрес для редиректа в случаем если оплата не состоялась
  $form['error_uri'] = array(
    '#type' => 'hidden',
    '#value' => url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
  );

 $form['process'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed to CloudPayments'),
  );

  return $form;
 
}


function commerce_cloudp_order_form($form, &$form_state, $order, $settings) {

  return $form;
}


function commerce_cloudp_redirect_form_validate($order, $payment_method) {
  // if (/* ... */) {
  //   return TRUE; // будет вызван callback PAYMENTNAME_redirect_form_submit()
  // }
  // else {
  //   return FALSE; // покупатель возвратится на шаг назад в форме чекаута
  // }
}
 
/**
 * Payment method callback: redirect form submission.
 */
function commerce_cloudp_redirect_form_submit($order, $payment_method) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
 
  // Создаём транзакцию
  $transaction = commerce_payment_transaction_new('commerce_cloudp', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $order_wrapper->commerce_order_total->amount->value();
  $transaction->currency_code = $order_wrapper->commerce_order_total->currency_code->value();
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->message = t('The payment commerce_cloudp has completed.');
  commerce_payment_transaction_save($transaction);
}


function commerce_cloudp_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order->data['commerce_cloudp'] = $pane_values;
  
}